package azstack.com.optimime.hospital.ui.service;

public enum Notify {
    DATA_PUSH("azstack.com.optimime.hospital.data.push"), UPDATE_STATUS("azstack.com.optimime.hospital.update.status");

    private String value;

    private Notify(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
